#import <Foundation/Foundation.h>
#import "ExampleTwo.h"
#import <UIKit/UIKit.h>

extern UIViewController* UnityGetGLViewController(void);
extern void UnitySendMessage(const char *, const char *, const char *);

void NativePickColor(char* message, char* optionOne, char* optionTwo)
{
	NSString *nsMessage = [NSString stringWithFormat:@"%s", message];
	NSString *nsOptionOne = [NSString stringWithFormat:@"%s", optionOne];
	NSString *nsOptionTwo = [NSString stringWithFormat:@"%s", optionTwo];

	UIAlertController * alert = [UIAlertController
								 alertControllerWithTitle:nil
								 message:nsMessage
								 preferredStyle:UIAlertControllerStyleAlert];

	UIAlertAction* optionOneButton = [UIAlertAction
									  actionWithTitle:nsOptionOne
									  style:UIAlertActionStyleDefault
									  handler:^(UIAlertAction * action) {
										  UnitySendMessage("ExampleTwo", "ResponseFromNative", [nsOptionOne UTF8String]);
									  }];

	UIAlertAction* optionTwoButton = [UIAlertAction
									  actionWithTitle:nsOptionTwo
									  style:UIAlertActionStyleDefault
									  handler:^(UIAlertAction * action) {
										  UnitySendMessage("ExampleTwo", "ResponseFromNative", [nsOptionTwo UTF8String]);
									  }];

	[alert addAction:optionOneButton];
	[alert addAction:optionTwoButton];

	UIViewController *rootViewController = UnityGetGLViewController();
	[rootViewController presentViewController:alert animated:YES completion:nil];
}

