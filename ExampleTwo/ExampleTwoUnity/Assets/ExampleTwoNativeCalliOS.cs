﻿using System.Runtime.InteropServices;

class ExampleTwoNativeCalliOS : IExampleTwoNativeCall
{
	[DllImport("__Internal")]
	public static extern void NativePickColor(string message, string optionOne, string optionTwo);

	public void PickColor(string message, string optionOne, string optionTwo)
	{
		NativePickColor(message, optionOne, optionTwo);
	}
}