﻿using UnityEngine;

class ExampleTwoNativeCallEditor : IExampleTwoNativeCall
{
	public void PickColor(string message, string optionOne, string optionTwo)
	{
		GameObject go = GameObject.Find("/ExampleTwo");
		go.SendMessage("ResponseFromNative", Random.Range(0,2) == 0 ? optionOne : optionTwo);
	}
}