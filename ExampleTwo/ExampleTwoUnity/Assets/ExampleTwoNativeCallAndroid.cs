﻿using UnityEngine;

class ExampleTwoNativeCallAndroid : IExampleTwoNativeCall
{
	public void PickColor(string message, string optionOne, string optionTwo)
	{
		var nativePlugin = new AndroidJavaObject("com.presentation.exampletwo.ExampleTwo");
		nativePlugin.Call("NativePickColor", message, optionOne, optionTwo);
	}
}