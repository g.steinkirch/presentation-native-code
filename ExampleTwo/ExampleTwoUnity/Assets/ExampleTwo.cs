﻿using UnityEngine;

public class ExampleTwo : MonoBehaviour
{
	public GameObject greenGO;
	public GameObject blueGO;

	private IExampleTwoNativeCall _exampleTwoNativeCalls;

	private void Start()
	{
#if UNITY_EDITOR
		_exampleTwoNativeCalls = new ExampleTwoNativeCallEditor();
#elif UNITY_ANDROID
		_exampleTwoNativeCalls = new ExampleTwoNativeCallAndroid();
#elif UNITY_IOS
		_exampleTwoNativeCalls = new ExampleTwoNativeCalliOS();
#endif
	}

	public void PickColor()
	{
		_exampleTwoNativeCalls.PickColor("Do you prefer green or blue?", "GREEN", "BLUE");
	}

	public void ResponseFromNative(string color)
	{
		if (color == "GREEN")
		{
			greenGO.SetActive(true);
			blueGO.SetActive(false);
		}
		else if (color == "BLUE")
		{
			greenGO.SetActive(false);
			blueGO.SetActive(true);
		}
	}
}