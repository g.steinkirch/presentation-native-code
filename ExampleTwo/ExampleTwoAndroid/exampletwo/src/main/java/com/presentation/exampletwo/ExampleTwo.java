package com.presentation.exampletwo;


import android.app.AlertDialog;
import android.content.DialogInterface;

import com.unity3d.player.UnityPlayer;

public class ExampleTwo {

	private String optionOne;
	private String optionTwo;

	public void NativePickColor(String message, String optOne, String optTwo)
	{
		this.optionOne = optOne;
		this.optionTwo = optTwo;

		AlertDialog.Builder builder = new AlertDialog.Builder(UnityPlayer.currentActivity);

		builder.setMessage(message);
		builder.setPositiveButton(optionOne, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which) {
				UnityPlayer.UnitySendMessage("ExampleTwo", "ResponseFromNative", optionOne);
				dialog.dismiss();
			}
		});

		builder.setNegativeButton(optionTwo, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				UnityPlayer.UnitySendMessage("ExampleTwo", "ResponseFromNative", optionTwo);
				dialog.dismiss();
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
