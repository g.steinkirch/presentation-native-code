﻿using UnityEngine;
using UnityEngine.UI;

public class ExampleThree : MonoBehaviour
{
	private IExampleThreeNativeCall _exampleThreeNativeCalls;
	public Text ResultLabel;

	private void Start()
	{
#if UNITY_EDITOR
		_exampleThreeNativeCalls = new ExampleThreeNativeCallEditor();
#elif UNITY_IOS
		_exampleThreeNativeCalls = new ExampleThreeNativeCalliOS();
#elif UNITY_ANDROID
		_exampleThreeNativeCalls = new ExampleThreeNativeCallAndroid();
#endif
	}

	public void CallNative()
	{
		_exampleThreeNativeCalls.CallNative(ResultLabel);
	}
}
