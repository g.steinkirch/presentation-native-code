#ifndef ExampleThree_h
#define ExampleThree_h

typedef void (*MyCallbackDelegate)(char* message);
void UsingCallbacks(MyCallbackDelegate myCallbackDelegate);

#endif
