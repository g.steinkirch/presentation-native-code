﻿using UnityEngine.UI;

public interface IExampleThreeNativeCall
{
	void CallNative(Text label);
}