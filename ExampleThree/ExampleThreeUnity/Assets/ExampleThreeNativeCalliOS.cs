﻿using System.Runtime.InteropServices;
using AOT;
using UnityEngine.UI;

class ExampleThreeNativeCalliOS : IExampleThreeNativeCall
{
	public delegate void MyCallbackDelegate(string message);
	private static Text Label;

	[DllImport("__Internal")]
	public static extern void UsingCallbacks(MyCallbackDelegate callback);

	public void CallNative(Text label)
	{
		Label = label;
		UsingCallbacks(MyCallback);
	}

	[MonoPInvokeCallback(typeof(MyCallbackDelegate))]
	private static void MyCallback(string message)
	{
		Label.text = message;
	}
}
