﻿using System;
using UnityEngine;
using UnityEngine.UI;

class ExampleThreeAndroidCallback : AndroidJavaProxy
{
	public ExampleThreeAndroidCallback() : base("com.presentation.examplethree.IExampleThreeAndroidCallback") {}
	public Text Label;
	
	public void MyCallback(String message)
	{
		Label.text = message;
	}
}
