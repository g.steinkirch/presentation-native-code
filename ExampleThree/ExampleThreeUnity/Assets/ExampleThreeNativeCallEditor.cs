﻿using UnityEngine.UI;

class ExampleThreeNativeCallEditor : IExampleThreeNativeCall
{
	public void CallNative(Text label)
	{
		label.text = "Hello from Editor!";
	}
}
