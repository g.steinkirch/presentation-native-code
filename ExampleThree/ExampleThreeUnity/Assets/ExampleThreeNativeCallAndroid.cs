﻿using UnityEngine;
using UnityEngine.UI;

class ExampleThreeNativeCallAndroid : IExampleThreeNativeCall
{
	public void CallNative(Text label)
	{
		var nativePlugin = new AndroidJavaObject("com.presentation.examplethree.ExampleThree");

		var callback = new ExampleThreeAndroidCallback();
		callback.Label = label;

		nativePlugin.Call("UsingCallbacks", callback);
	}
}
