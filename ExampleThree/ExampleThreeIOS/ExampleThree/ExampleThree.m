#import <Foundation/Foundation.h>
#import "ExampleThree.h"

typedef void (*MyCallbackDelegate)(char* message);

void UsingCallbacks(MyCallbackDelegate myCallbackDelegate)
{
	myCallbackDelegate("Hello from Native!");
}
