#import <Foundation/Foundation.h>

@interface SimpleToast : NSObject

+(void)showToast:(NSString*)message;

@end

