#import <UIKit/UIKit.h>
#import "SimpleToast.h"

@implementation SimpleToast

+(void)showToast:(NSString*)message
{
	UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
						  message:message
						  delegate:nil
						  cancelButtonTitle:nil
						  otherButtonTitles:nil, nil];
	[toast show];
	int duration = 2;

	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
		[toast dismissWithClickedButtonIndex:0 animated:YES];
	});
}

@end
