#import <Foundation/Foundation.h>
#import "SimpleToast.h"

void NativeShowToast(char* message)
{
	NSString *nsMessage = [NSString stringWithFormat:@"%s", message];
	[SimpleToast showToast:nsMessage];
}
