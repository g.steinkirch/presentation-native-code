﻿using UnityEngine;

class ExampleOneNativeCallAndroid : IExampleOneNativeCall
{
	public void ShowToast(string message)
	{
		var nativeClass = new AndroidJavaObject("com.presentation.exampleone.ExampleOne");
		nativeClass.Call("NativeShowToast", message);
	}
}