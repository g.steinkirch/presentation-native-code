﻿using System.Runtime.InteropServices;

class ExampleOneNativeCalliOS : IExampleOneNativeCall
{
	[DllImport("__Internal")]
	public static extern void NativeShowToast(string message);

	public void ShowToast(string message)
	{
		NativeShowToast(message);
	}
}