﻿using UnityEngine;

public class ExampleOne : MonoBehaviour
{
	private IExampleOneNativeCall _exampleOneNativeCalls;

	private void Start()
	{
#if UNITY_EDITOR
		_exampleOneNativeCalls = new ExampleOneNativeCallEditor();
#elif UNITY_ANDROID
		_exampleOneNativeCalls = new ExampleOneNativeCallAndroid();
#elif UNITY_IOS
		_exampleOneNativeCalls = new ExampleOneNativeCalliOS();
#endif
	}

	public void ShowToast()
	{
		_exampleOneNativeCalls.ShowToast("Hello from Unity!");
	}
}