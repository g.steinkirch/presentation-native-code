﻿using UnityEngine;

class ExampleOneNativeCallEditor : IExampleOneNativeCall
{
	public void ShowToast(string message)
	{
		Debug.LogFormat("Show Toast with message: {0}", message);
	}
}