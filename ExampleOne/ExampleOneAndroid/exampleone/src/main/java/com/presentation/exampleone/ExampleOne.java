package com.presentation.exampleone;

import android.widget.Toast;

import com.unity3d.player.UnityPlayer;

public class ExampleOne {

	public void NativeShowToast(String message) {
		Toast.makeText(UnityPlayer.currentActivity, message, Toast.LENGTH_SHORT).show();
	}
}
